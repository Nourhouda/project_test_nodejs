var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')
const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
    }
   
});
function create_table(){
console.log('create table')
    db.run(`CREATE TABLE utilisateur (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        firstname text, 
        lastname text, 
        email text UNIQUE, 
        password text, 
        userKey text,
        active INTEGER
        )`,
   
    )
}
function insert_data(firstname,lastname,email,password,userKey,active){
      // Table just created, creating some rows
      var insert = 'INSERT INTO utilisateur (firstname,lastname, email, password,userKey,active) VALUES (?,?,?,?,?,?)'
      db.run(insert,firstname,lastname,email,password,userKey,active,function(err) {
        if (err) {
            console.log('error')
            console.log(err)
          return console.log(err.message);
        }else{
            console.log('insert data with success')
        }
    });
     
 
}
function get_user_by_mail(email){

 let sql = `SELECT * FROM utilisateur where email=?`;
var res
db.all(sql, email, (err, rows) => {
if (err) {
console.log('error')
console.log(err)
}
res=rows[0]

});
return res
}
function get_user_by_key(userkey){
    let sql = `SELECT * FROM utilisateur where userKey=?`;

    db.all(sql, userkey, (err, rows) => {
    if (err) {
    console.log('error')
    console.log(err)
    }
    rows.forEach((row) => {
     console.log('row data')
    console.log(row);
    });
    });

}

function update_user(data){
    console.log('data')
    console.log(data)
    let sql = `UPDATE utilisateur
            SET active = ?
            WHERE userKey = ?`;

db.run(sql, data, function(err) {
  if (err) {
    return console.error(err.message);
  }
  console.log(`Row(s) updated: ${this.changes}`);

});

// close the database connection
db.close();

}
module.exports = {db,create_table,insert_data,get_user_by_mail,get_user_by_key,update_user}
