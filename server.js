// Create express app
var express = require("express")
var app = express()
const db1=require('./database')
const mail=require('./mail')
// Server ports
var HTTP_PORT = 8999 
// Start server
app.listen(HTTP_PORT, () => {
    console.log('server info')
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
app.get("/", (req, res, next) => {
    console.log('ok')
   return res.json({"message":"nour"})
});

// Root endpoint
app.get("/create-table", (req, res, next) => {
    console.log('ok')
    db1.create_table()
   return res.json({"message":"create table"})
});


// Insert here other API endpoints
app.post("/add-user", (req, res, next) => {
    try{
    req.on('data', function (data) 
    { 
        var msg = JSON.parse(data)
        db1.insert_data(msg.firstname.toString(),msg.lastname.toString(),msg.email.toString(),msg.password.toString(),msg.userKey.toString(),msg.active)
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify({"status":"ok",'response':'insert with success'}));
        res.end();                         
    })
}catch(err){
    res.writeHead(400, {'Content-Type': 'text/html'});
    res.write(JSON.stringify({"status":"Failure","response":err}));
    res.end();  
}
});
//get user by email
app.post("/api/user-email", (req, res, next) => {
  try{
    req.on('data', function (data) 
    {
        var msg = JSON.parse(data)
      var user= db1.get_user_by_mail(msg.email.toString()) 
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write(JSON.stringify({"status":"ok",'response':user}));
      res.end();    
    })
}catch(err){
    res.writeHead(400, {'Content-Type': 'text/html'});
    res.write(JSON.stringify({"status":"Failure","response":err}));
    res.end();  
}
    
});
//update user account
app.post("/api/user-update", (req, res, next) => {
  try{
    req.on('data', function (data) 
    {
        var msg = JSON.parse(data)
         db1.update_user([msg.active,msg.userKey])
       res.writeHead(200, {'Content-Type': 'text/html'});
       res.write(JSON.stringify({"status":"ok"}));
       res.end();      
    })

}catch(err){
    console.log('error')
    console.log(err)
    res.writeHead(400, {'Content-Type': 'text/html'});
    res.write(JSON.stringify({"status":"Failure","response":err}));
    res.end();  
}
     
});

// send mail
app.post("/api/send-mail", (req, res, next) => {
  try{
    req.on('data', function (data) 
    {
        var msg = JSON.parse(data)
        mail.send_mail(msg.userKey,msg.mail,html)
       res.writeHead(200, {'Content-Type': 'text/html'});
       res.write(JSON.stringify({"status":"ok","response":'send with success'}));
       res.end();      
    })

}catch(err){
    console.log(err)
    res.writeHead(400, {'Content-Type': 'text/html'});
    res.write(JSON.stringify({"status":"Failure","response":err}));
    res.end();  
}
     
});
// Default response for any other request
app.use(function(req, res){
    res.status(404);
});